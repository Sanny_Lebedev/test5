package URL

import (
	"encoding/json"
	"net/http"
)

func wrongMethod() SysAnswer {
	return SysAnswer{
		Success: false,
		Message: AnswerTypeIndex(AnswerTypeForbidden).Title(),
	}
}

func notFound() SysAnswer {
	return SysAnswer{
		Success: false,
		Message: AnswerTypeIndex(AnswerTypeNotFound).Title(),
	}
}

func sendError(err Error) SysAnswer {
	return SysAnswer{
		Success:      false,
		Message:      err.Message,
		MessageError: err.Error.Error(),
	}
}

func sendList(in interface{}) SysAnswer {
	return SysAnswer{
		Success: true,
		Message: AnswerTypeName[AnswerTypeOk],
		Meta:    in,
	}
}

func sendOnConstr() SysAnswer {
	return SysAnswer{
		Success: false,
		Message: AnswerTypeName[AnswerTypeUnderCNS],
	}
}

// BadRequest - ошибка
func (a SysAnswer) BadRequest(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadGateway)
	json.NewEncoder(w).Encode(a)
}

// Forbidden - запрет
func (a SysAnswer) Forbidden(w http.ResponseWriter) {
	w.WriteHeader(http.StatusForbidden)
	json.NewEncoder(w).Encode(a)
}

// NotFound - не найдено
func (a SysAnswer) NotFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(a)
}

// Ok - не найдено
func (a SysAnswer) Ok(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(a)
}
