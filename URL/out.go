package URL

import (
	"bitbucket.org/Sanny_Lebedev/test5/datas"
)

// Out ...
func (p GetNotFound) Out() error {
	notFound().NotFound(p.Answer.Net.w)
	return nil
}

// Out ...
func (p PostSettings) Out() error {
	if p.Answer.Error.HasError {
		sendError(p.Answer.Error).BadRequest(p.Params.Net.w)
		return nil
	}

	ans, err := datas.Init(p.Params.c.db, p.Params.c.log).InsertSetting(datas.SettingsItem{
		Link:  p.Params.SettingsItem.Link,
		Title: p.Params.SettingsItem.Title,
		Tags:  datas.Tags(p.Params.SettingsItem.Tags),
		Date:  p.Params.SettingsItem.Date})

	if err != nil {
		sendError(Error{HasError: true, Message: AnswerTypeName[6], Error: err}).BadRequest(p.Params.Net.w)
		return err
	}
	sendList(ans).Ok(p.Params.Net.w)
	return nil

}

// Out ...
func (p PostSearch) Out() error {
	if p.Answer.Error.HasError {
		sendError(p.Answer.Error).BadRequest(p.Params.Net.w)
	}

	ans, err := datas.Init(p.Params.c.db, p.Params.c.log).SetSearch(p.Params.Search).SetPaginator(datas.Paginator(p.Params.Paginator)).Make()	
	if err != nil {
		sendError(Error{HasError: true, Message: AnswerTypeName[AnswerTypeError], Error: err}).BadRequest(p.Params.Net.w)
		return err
	}
	sendList(ans).Ok(p.Params.Net.w)
	return nil
}

// Out ...
func (p GetSettings) Out() error {
	ans, err := datas.Init(p.Params.c.db, p.Params.c.log).GetSettings(p.Params.UID)
	if err != nil {
		sendError(Error{HasError: true, Message: AnswerTypeName[6], Error: err}).BadRequest(p.Params.Net.w)
		return err
	}
	sendList(ans).Ok(p.Params.Net.w)
	return nil
}

// Out ...
func (p DeleteSettings) Out() error {
	if p.Answer.Error.HasError {
		sendError(p.Answer.Error).BadRequest(p.Params.Net.w)
	}
	ans, err := datas.Init(p.Params.c.db, p.Params.c.log).DeleteSetting(p.Params.UID)
	if err != nil {
		sendError(Error{HasError: true, Message: AnswerTypeName[6], Error: err}).BadRequest(p.Params.Net.w)
		return err
	}
	sendList(ans).Ok(p.Params.Net.w)
	return nil
}
