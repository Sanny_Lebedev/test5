package URL

import (
	"html"
	"net/http"

	"bitbucket.org/Sanny_Lebedev/test5/logger"
	"github.com/jackc/pgx"
)

// Init  - инициализация
func Init(db *pgx.ConnPool, log logger.Logger) *URL {
	return &URL{c: Config{db: db, log: log}}
}

// Set - установка пути
func (u *URL) Set(r *http.Request, w http.ResponseWriter) *URL {
	u.Path = u.Path.Set(html.EscapeString(r.URL.Path))
	u.isExist = u.checkPath()
	u.Method = r.Method
	u.Net = &Net{r: r, w: w}
	u.Struct = u.GetStruct()
	return u
}

func (u *URL) checkPath() bool {
	return u.Path.Check()
}

// GetStruct - получение структуры в соответствии с методом запроса
func (u *URL) GetStruct() CurrentMethod {
	switch u.Path {
	case Search:
		if u.Method == http.MethodPost {
			return u.c.initPostSearch(u.Net)
		}
		return u.c.initGetNotFound(u.Net)
	case Settings:
		if u.Method == http.MethodDelete {
			return u.c.initDeleteSettings(u.Net)
		}

		if u.Method == http.MethodGet {
			return u.c.initGetSettings(u.Net)
		}

		if u.Method == http.MethodPost {
			return u.c.initPostSettings(u.Net)
		}
		return u.c.initGetNotFound(u.Net)

	default:
		return u.c.initGetNotFound(u.Net)

	}

}

func (c *Config) postSearch(net *Net) CurrentMethod {
	var in CurrentMethod
	in = PostSearch{Params: SearchParams{c: c, Net: net}}
	return in
}

func (c *Config) initPostSearch(net *Net) CurrentMethod {
	return PostSearch{Params: SearchParams{c: c, Net: net}}
}

func (c *Config) initGetSettings(net *Net) CurrentMethod {
	return GetSettings{Params: SettingsParams{c: c, Net: net}}
}

func (c *Config) initDeleteSettings(net *Net) CurrentMethod {
	return DeleteSettings{Params: SettingsParams{c: c, Net: net}}
}

func (c *Config) initPostSettings(net *Net) CurrentMethod {
	return PostSettings{Params: SettingsPostParams{c: c, Net: net}}
}

func (c *Config) initGetNotFound(net *Net) CurrentMethod {
	c.log.Warn().Msg(AnswerTypeName[AnswerTypeNotFoundRes])
	return GetNotFound{Answer: NotFoundAnswer{Net: net, c: c, StatusCode: http.StatusNotFound}}
}
