package URL

import (
	"net/http"

	"bitbucket.org/Sanny_Lebedev/test5/logger"
	"github.com/jackc/pgx"
)

type CurrentMethod interface {
	SetParams() CurrentMethod
	Make() CurrentMethod
	Out() error
}

type (

	// SysAnswer - общая структура ответа
	SysAnswer struct {
		Success      bool        `json:"success"`
		Message      string      `json:"message"`
		MessageError string      `json:"errMessage,omitempty"`
		Meta         interface{} `json:"meta,omitempty"`
	}

	// URL ...
	URL struct {
		Path    CurrentURL    `json:"path"`
		Answer  interface{}   `json:"answer"`
		Error   Error         `json:"error,omitempty"`
		c       Config        `json:"-"`
		Net     *Net          `json:"-"`
		isExist bool          `json:"-"`
		Method  string        `json:"-"`
		Struct  CurrentMethod `json:"-"`
	}
	// Net ...
	Net struct {
		w http.ResponseWriter
		r *http.Request
	}

	// Error ...
	Error struct {
		Error      error  `json:"error"`
		Message    string `json:"message"`
		HasError   bool   `json:"-"`
		StatusCode int    `json:"-"`
	}
	// Config ...
	Config struct {
		db  *pgx.ConnPool
		log logger.Logger
	}
)
