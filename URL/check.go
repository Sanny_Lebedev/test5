package URL

// CheckParam ...
func (p *PostSettings) CheckParam() *PostSettings {
	if len(p.Params.Link) == 0 {
		p.Answer.Error.HasError = true
		p.Answer.Error.Message = AnswerTypeName[AnswerTypeNeedLink]
		return p
	}

	if len(p.Params.Tags.Content) == 0 {
		p.Answer.Error.HasError = true
		p.Answer.Error.Message = AnswerTypeName[AnswerTypeNeedTCont]
		return p
	}

	if len(p.Params.Tags.Message) == 0 {
		p.Answer.Error.HasError = true
		p.Answer.Error.Message = AnswerTypeName[AnswerTypeNeedTMsg]
		return p
	}

	if len(p.Params.Tags.Title) == 0 {
		p.Answer.Error.HasError = true
		p.Answer.Error.Message = AnswerTypeName[AnswerTypeNeedTTitle]
		return p
	}
	return p
}

// CheckParam ...
func (p *PostSearch) CheckParam() *PostSearch {
	if p.Params.Paginator.CurrentPage != 0 || p.Params.Paginator.OnPage != 0 {
		p.Params.Paginator.Paginate = true
		p.Params.Paginator.Offset = p.Params.Paginator.OnPage*p.Params.Paginator.CurrentPage - p.Params.Paginator.OnPage
	}

	return p
}

// CheckParam ...
func (p *GetSettings) CheckParam() *GetSettings {
	return p
}

// DeleteSettings ...
func (p *DeleteSettings) CheckParam() *DeleteSettings {
	if len(p.Params.UID) == 0 {
		p.Answer.Error.HasError = true
		p.Answer.Error.Message = AnswerTypeName[AnswerTypeNeedUID]
	}
	return p
}
