package URL

import "fmt"

// AnswerType - Тип ответа
type AnswerType string

// AnswerTypeTitle - Текст ответа
type AnswerTypeTitle string

// String - вернуть строку
func (at AnswerType) String() string {
	return string(at)
}

// Set - установить тип
func (rn *AnswerTypeTitle) Set(AnswerType string) {
	*rn = AnswerTypeTitle(AnswerType)
}

// String - вернуть строку
func (rn AnswerTypeTitle) String() string {
	return string(rn)
}

// AnswerTypeIndex ...
type AnswerTypeIndex uint

const (
	// AnswerTypeForbidden ...
	AnswerTypeForbidden = iota
	// AnswerTypeNotFound ...
	AnswerTypeNotFound
	// AnswerTypeNeedLink ...
	AnswerTypeNeedLink
	// AnswerTypeNeedTCont ...
	AnswerTypeNeedTCont
	// AnswerTypeNeedTMsg ...
	AnswerTypeNeedTMsg
	// AnswerTypeNeedTTitle ...
	AnswerTypeNeedTTitle
	// AnswerTypeError ...
	AnswerTypeError
	// AnswerTypeOk ...
	AnswerTypeOk
	// AnswerTypeNeedUID ...
	AnswerTypeNeedUID
	// AnswerTypeUnderCNS ...
	AnswerTypeUnderCNS
	// AnswerTypeNotFoundRes ...
	AnswerTypeNotFoundRes
)

// AnswerTypeName ...
var AnswerTypeName = map[AnswerTypeIndex]string{
	AnswerTypeForbidden:   "Доступ запрещен",
	AnswerTypeNotFound:    "По Вашему запросу ничего не найдено",
	AnswerTypeNeedLink:    "Необходим параметр Link",
	AnswerTypeNeedTCont:   "Необходим параметр Tags.Content",
	AnswerTypeNeedTMsg:    "Необходим параметр Tags.Message",
	AnswerTypeNeedTTitle:  "Необходим параметр Tags.Title",
	AnswerTypeError:       "Ошибка запроса",
	AnswerTypeOk:          "Результат запроса в meta",
	AnswerTypeNeedUID:     "Необходимо указать UID",
	AnswerTypeUnderCNS:    "Раздел в разработке",
	AnswerTypeNotFoundRes: "Метод не найден",
}

// Title - возврат заголовка
func (ati AnswerTypeIndex) Title() string {
	if text, ok := AnswerTypeName[ati]; ok {
		return text
	}
	return fmt.Sprintf("Status code %d", ati)
}
