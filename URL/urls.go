package URL

import "fmt"

// CurrentURL - путь
type CurrentURL string

// URLIndex ...
type URLIndex uint

const (

	// NotFountInd ...
	NotFountInd = iota
	// SettingInd ...
	SettingInd
	// SearchInd ...
	SearchInd
)

const (
	NotFound CurrentURL = "/notfound"
	Settings CurrentURL = "/settings"
	Search   CurrentURL = "/search"
)

// URLPathName ...
var URLPathName = map[URLIndex]CurrentURL{
	NotFountInd: NotFound,
	SettingInd:  Settings,
	SearchInd:   Search,
}

// String - вернуть строку
func (s CurrentURL) String() string {
	return string(s)
}

// Set - вернуть строку
func (s CurrentURL) Set(url string) CurrentURL {
	return CurrentURL(url)
}

// Title - возврат заголовка
func (ati URLIndex) Title() string {
	if text, ok := URLPathName[ati]; ok {
		return string(text)
	}
	return fmt.Sprintf("Status code %d", ati)
}

// Check - сheck the existence of AppTypeIndex by name
func (s CurrentURL) Check() bool {
	index, isOk := s.GetIndex()
	if !isOk {
		return false
	}

	_, ok := URLPathName[index]
	return ok
}

// GetIndex - get apptype index by title
func (s CurrentURL) GetIndex() (URLIndex, bool) {
	for ind, val := range URLPathName {
		if val == s {
			return URLIndex(ind), true
		}
	}
	return URLIndex(0), false
}
