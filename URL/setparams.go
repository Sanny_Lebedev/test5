package URL

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// SetParams - установка параметров для метода GET notfound
func (p GetNotFound) SetParams() CurrentMethod {
	p.Answer.StatusCode = http.StatusNotFound
	p.Answer.Error.Message = "Not found"
	p.Answer.Error.StatusCode = http.StatusNotFound
	return p
}

// SetParams - установка параметров для меотда POST settings
func (p PostSettings) SetParams() CurrentMethod {
	body, readErr := ioutil.ReadAll(p.Params.Net.r.Body)
	if readErr != nil {
		p.Params.c.log.Error().Err(readErr).Msg("Error")
		http.Error(p.Params.Net.w, "Error.", http.StatusBadRequest)
	}

	json.Unmarshal(body, &p.Params)

	return p
}

// SetParams - установка параметров для метода POST search
func (p PostSearch) SetParams() CurrentMethod {

	body, readErr := ioutil.ReadAll(p.Params.Net.r.Body)
	if readErr != nil {
		p.Params.c.log.Error().Err(readErr).Msg("Error")
		http.Error(p.Params.Net.w, "Error.", http.StatusBadRequest)
	}
	json.Unmarshal(body, &p.Params)
	return p
}

// SetParams - установка параметров для GET метода settings
func (p GetSettings) SetParams() CurrentMethod {
	p.Params.UID = getUIDParam(p.Params.Net.r, "UID")
	return p
}

// SetParams - установка параметров для DELETE метода settings
func (p DeleteSettings) SetParams() CurrentMethod {
	p.Params.UID = getUIDParam(p.Params.Net.r, "UID")
	return p
}

// Запрос параметров из URL
func getUIDParam(r *http.Request, param string) string {
	keys, ok := r.URL.Query()[param]
	if !ok || len(keys[0]) < 1 {
		return ""
	}
	return keys[0]
}
