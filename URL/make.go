package URL

// Make ...
func (p GetNotFound) Make() CurrentMethod {
	return p
}

// Make ...
func (p PostSettings) Make() CurrentMethod {
	if p.Answer.StatusCode != 0 || p.Answer.Error.HasError {
		return p
	}

	return *p.CheckParam()
}

// Make ...
func (p PostSearch) Make() CurrentMethod {

	if p.Answer.StatusCode != 0 || p.Answer.Error.HasError {
		return p
	}

	return *p.CheckParam()
}

// Make ...
func (p GetSettings) Make() CurrentMethod {
	return p
}

// Make ...
func (p DeleteSettings) Make() CurrentMethod {
	return p
}
