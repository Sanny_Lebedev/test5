package URL

import "time"

type (

	// PostSearch - структура для POST запроса к методу Search
	PostSearch struct {
		Params SearchParams
		Answer SearchAnswer
	}

	// AnswerNotFound - структура для GET запроса к методу settings
	GetNotFound struct {
		Answer NotFoundAnswer
	}

	// GetSettings - структура для GET запроса к методу settings
	GetSettings struct {
		Params SettingsParams
		Answer SettingsAnswer
	}

	// DeleteSettings - структура для DELETE запроса к методу settings
	DeleteSettings struct {
		Params SettingsParams
		Answer SettingsAnswer
	}

	// GetSettings - структура для GET запроса к методу settings
	PostSettings struct {
		Params SettingsPostParams
		Answer SettingsAnswer
	}

	// SearchParams - параметры POST запроса метода Search
	SearchParams struct {
		c         *Config
		Net       *Net
		Paginator Paginator `json:"paginator"`
		Search    string    `json:"search"`
	}

	// SettingsParams - параметры POST запроса метода settings
	SettingsParams struct {
		c   *Config
		Net *Net
		UID string `json:"UID"`
	}

	// SettingsPostParams - параметры POST запроса метода settings
	SettingsPostParams struct {
		c   *Config
		Net *Net
		SettingsItem
	}

	// Paginator - структура для работы пагинатора
	Paginator struct {
		Paginate    bool
		Offset      int
		OnPage      int `json:"onPage"`
		CurrentPage int `json:"currentPage"`
	}

	// SearchAnswer - ответ для метода Search
	SearchAnswer struct {
		List       []SearchItem `json:"list"`
		Count      int          `json:"count"`
		Total      int          `json:"total"`
		HasNext    bool         `json:"hasNext"`
		Error      Error        `json:"error,omitempty"`
		StatusCode int
	}

	// NotFoundAnswer - ответ notfound
	NotFoundAnswer struct {
		Error      Error `json:"error,omitempty"`
		Net        *Net
		c          *Config
		StatusCode int
	}

	// SearchItem - элементы ответа (статьи)
	SearchItem struct {
		UID   string    `json:"UID"`
		Title string    `json:"title"`
		Body  string    `json:"body"`
		Date  time.Time `json:"date"`
	}

	// SettingsAnswer - ответ для метода GET settings
	SettingsAnswer struct {
		List       []SearchItem `json:"list"`
		Count      int          `json:"count"`
		Total      int          `json:"total"`
		HasNext    bool         `json:"hasNext"`
		Error      Error        `json:"error,omitempty"`
		StatusCode int
	}

	// SettingsItem - элемент настройки
	SettingsItem struct {
		UID   string    `json:"UID"`
		Link  string    `json:"link"`
		Title string    `json:"title"`
		Tags  Tags      `json:"tags"`
		Date  time.Time `json:"date"`
	}

	// Tags - структура настройки тегов
	Tags struct {
		Content string `json:"content"`
		Title   string `json:"title"`
		Message string `json:"message"`
	}
)
