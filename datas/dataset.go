package datas

import (
	"time"

	"bitbucket.org/Sanny_Lebedev/test5/logger"
	"github.com/jackc/pgx"
)

type (
	// Config ...
	Config struct {
		db  *pgx.ConnPool
		log logger.Logger
	}

	// SearchParams - параметры поиска
	SearchParams struct {
		c         *Config
		Paginator Paginator
		Search    string
	}

	// SettingsParams - параметры POST запроса метода settings
	SettingsParams struct {
		c   *Config
		UID string `json:"UID"`
	}

	// // SettingsPostParams - параметры POST запроса метода settings
	// SettingsPostParams struct {
	// 	c *Config
	// 	SettingsItem
	// }

	// Paginator - структура для работы пагинатора
	Paginator struct {
		Paginate    bool `json:"-"`
		Offset      int  `json:"-"`
		OnPage      int  `json:"onPage"`
		CurrentPage int  `json:"currentPage"`
	}

	// SearchAnswer - ответ для метода Search
	SearchAnswer struct {
		List       []SearchItem `json:"list"`
		Count      int          `json:"count"`
		Total      int          `json:"total"`
		HasNext    bool         `json:"hasNext"`
		Error      Error        `json:"error,omitempty"`
		StatusCode int          `json:"-"`
	}
	// Error - структура хранения данных про ошибку
	Error struct {
		Error      error  `json:"error"`
		Message    string `json:"message"`
		HasError   bool   `json:"-"`
		StatusCode int    `json:"-"`
	}
	// SearchItem - элементы ответа (статьи)
	SearchItem struct {
		UID    string    `json:"UID"`
		Source string    `json:"sourceTitle"`
		Link   string    `json:"link"`
		Title  string    `json:"title"`
		Body   string    `json:"body"`
		Date   time.Time `json:"date"`
	}

	// SettingsAnswer - ответ для метода GET settings
	SettingsAnswer struct {
		List       []SettingsItem `json:"list"`
		Count      int            `json:"count"`
		Total      int            `json:"total"`
		HasNext    bool           `json:"hasNext"`
		Error      Error          `json:"error,omitempty"`
		StatusCode int            `json:"-"`
	}

	// SettingsItem - элемент настройки
	SettingsItem struct {
		UID   string    `json:"UID"`
		Link  string    `json:"link"`
		Title string    `json:"title"`
		Tags  Tags      `json:"tags"`
		Date  time.Time `json:"date"`
	}

	// Tags - структура настройки тегов
	Tags struct {
		Content string `json:"content"`
		Title   string `json:"title"`
		Message string `json:"message"`
	}

	// SQL - структура для поиска
	SQL struct {
		Main  string
		Where string
		Limit string
		Order string
		Args  []interface{}
	}
)
