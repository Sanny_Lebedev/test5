package datas

import "fmt"

// SetSearch - инициализация структуры поиска
func (c *Config) SetSearch(search string) *SearchParams {
	return &SearchParams{Search: search, c: c}

}

// SetPaginator - установить настройки пагинации
func (c *SearchParams) SetPaginator(paginator Paginator) *SearchParams {
	c.Paginator = paginator
	return c
}

// Make - реализация поиска
func (c *SearchParams) Make() (SearchAnswer, error) {
	sql := c.makeParams()

	out, err := c.c.makeSearch(sql)
	if err != nil {
		return SearchAnswer{}, err
	}

	if c.Paginator.Paginate {
		if c.Paginator.CurrentPage*c.Paginator.OnPage < out.Total {
			out.HasNext = true
		}
	}
	return out, nil
}

func (c *Config) makeSearch(s SQL) (SearchAnswer, error) {
	var out SearchAnswer
	rows, err := c.db.Query(fmt.Sprintf(s.Main, s.Where, s.Order, s.Limit), s.Args...)
	if err != nil {
		c.log.Error().Err(err).Msg("Ошибка в makeSearch")
		return SearchAnswer{}, err
	}
	var total int
	for rows.Next() {
		item := SearchItem{}
		err := rows.Scan(&total, &item.UID, &item.Source, &item.Link, &item.Title, &item.Body, &item.Date)
		if err != nil {
			c.log.Error().Err(err).Msg("Ошибка в SCAN makeSearch")
			return SearchAnswer{}, err
		}
		out.List = append(out.List, item)
	}
	out.Count = len(out.List)
	out.Total = total
	out.HasNext = false
	return out, nil
}

func (c *SearchParams) makeParams() SQL {
	var sql SQL
	sql.Main = `SELECT count(news.uid) OVER() AS total_count, news.uid, sourceslinks.sourcetitle, news.link, news.title, news.newsbody, news.data FROM news
				LEFT JOIN sourceslinks on sourceslinks.uid = news.source
				WHERE source <> '00000000-0000-0000-0000-000000000000' %v %v %v`
	sql.Order = `ORDER BY news.data DESC`

	if len(c.Search) > 0 {
		sql.Args = append(sql.Args, "%"+c.Search+"%")
		sql.Where = fmt.Sprintf(`AND (news.title) ILIKE ($%d)`, len(sql.Args))
	}

	if c.Paginator.Paginate {
		sql.Limit = fmt.Sprintf(`LIMIT $%d OFFSET $%d`, len(sql.Args)+1, len(sql.Args)+2)
		sql.Args = append(sql.Args, c.Paginator.OnPage, c.Paginator.Offset)
	}
	return sql
}
