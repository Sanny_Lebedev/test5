package datas

import (
	"bitbucket.org/Sanny_Lebedev/test5/logger"
	"github.com/jackc/pgx"
)

// Init  - инициализация
func Init(db *pgx.ConnPool, log logger.Logger) *Config {
	return &Config{db: db, log: log}
}
