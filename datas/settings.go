package datas

import (
	"fmt"
	"net/http"

	"github.com/jackc/pgx"
)

// DeleteSetting - удаление элемента из настроек
func (c *Config) DeleteSetting(UID string) (SettingsAnswer, error) {
	_, err := c.db.Exec(`DELETE FROM sourceslinks WHERE uid = $1`, UID)
	if err != nil {
		c.log.Error().Err(err).Msg("Ошибка удаления элемента из настроек")
		return SettingsAnswer{}, err
	}
	return c.GetSettings("")
}

// InsertSetting - добавить элементы настроек
func (c *Config) InsertSetting(item SettingsItem) (SettingsAnswer, error) {
	_, err := c.db.Exec(`INSERT INTO sourceslinks (sourcelink, sourcetitle, titleteg, msgteg, contentteg, date)
	 VALUES ($1, $2, $3, $4, $5, CURRENT_TIMESTAMP)`, item.Link, item.Title, item.Tags.Title, item.Tags.Message, item.Tags.Content)
	if err != nil {
		c.log.Error().Err(err).Msg("Ошибка сохранения элемента настроек")
		return SettingsAnswer{}, err
	}
	return c.GetSettings("")
}

// GetSettings - получить список настроек
func (c *Config) GetSettings(UID string) (SettingsAnswer, error) {
	var out SettingsAnswer
	var rows *pgx.Rows
	var err error
	const SQL = `SELECT uid, sourcelink, sourcetitle, titleteg, msgteg, contentteg, date FROM sourceslinks`
	Where := ""
	if len(UID) > 0 {
		Where = "WHERE uid = $1"
		rows, err = c.db.Query(fmt.Sprintf("%v %v", SQL, Where), UID)
	} else {
		rows, err = c.db.Query(SQL)
	}
	if err == pgx.ErrNoRows {
		return SettingsAnswer{HasNext: false, Count: 0, Total: 0, StatusCode: 0}, nil
	}
	if err != nil {
		c.log.Error().Err(err).Msg("Ошибка в GetSettings в SQL")
		return SettingsAnswer{}, err
	}

	for rows.Next() {
		item := SettingsItem{}
		err := rows.Scan(&item.UID, &item.Link, &item.Title, &item.Tags.Title, &item.Tags.Message, &item.Tags.Content, &item.Date)
		if err != nil {
			c.log.Error().Err(err).Msg("Ошибка в GetSettings при сканировании")
			return SettingsAnswer{}, err
		}
		out.List = append(out.List, item)
	}

	out.Count = len(out.List)
	out.Total = len(out.List)
	out.HasNext = false
	out.StatusCode = http.StatusOK
	return out, nil
}
