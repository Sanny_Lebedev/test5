package main

import (
	"io"

	"bitbucket.org/Sanny_Lebedev/test5/logger"
	"github.com/jackc/pgx"
)

type (
	// Структура для доступа к БД
	postgres struct {
		Host     string `json:"host" cfg:"host" cfgDefault:"example.com"`
		Port     int    `json:"port" cfg:"port" cfgDefault:"999"`
		Password string `json:"password" cfg:"password" cfgDefault:"123"`
		Username string `json:"username" cfg:"username" cfgDefault:"username"`
		Database string `json:"database" cfg:"database" cfgDefault:"database"`
	}

	// Структура для системного лога
	systemLog struct {
		Level   string `json:"level" cfg:"level"`
		Path    string `json:"path" cfg:"path"`
		File    string `json:"file" cfg:"file"`
		FileRes string `json:"fileres" cfg:"fileres"`
		Mode    string `json:"mode" cfg:"mode"`
	}

	// Структура основной конфигурации
	configMain struct {
		Logparam systemLog `json:"log" cfg:"log"`
		Postgres postgres  `json:"postgres" cfg:"postgres"`
	}

	//Config - конфигурация
	Config struct {
		DB   *pgx.ConnPool
		Log  logger.Logger
		file io.WriteCloser
	}

	//Env - Структура конфигурации системы
	Env struct {
		myConf *configMain
	}
)
