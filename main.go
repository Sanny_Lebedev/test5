package main

import (
	"net/http"

	localURL "bitbucket.org/Sanny_Lebedev/test5/URL"
)

// Close - close log file
func (c *Config) Close() {
	if c.file != nil {
		c.file.Close()
	}
}

func main() {
	conf := initial()
	defer conf.Close()
	conf.Log.Info().Bool("status", true).Msg("Initialization")
	url := localURL.Init(conf.DB, conf.Log)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		url.Set(r, w)
		url.Struct.SetParams().Make().Out()
		conf.Log.Info().Str("Method", r.Method).Str("URL", r.URL.Path).Str("IP", ReadUserIP(r)).Msg("Get task")
	})
	conf.Log.Error().Err(http.ListenAndServe(":1160", nil)).Msg("Test")
}

// ReadUserIP - получение адреса отправителя
func ReadUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}
